/* 
 * WebMap Plugin v1.0 
 * @author      Melquisedec Wilchez <CompuDev Web & Hosting>
 */
var mapa = null;

(function($){
    $.fn.extend({
        initMap: function(bound, srs, unit, layers){
            return this.each(function(){
                var zona = new OpenLayers.Bounds(bound);
                mapa = new OpenLayers.Map({
                    div: this,
                    projection: srs,
                    units: unit,
                    maxResolution: 186.663920,
                    maxExtent: zona
                });
            });
        },
        setControls: function(controls){
    
        }
    });
})(jQuery);