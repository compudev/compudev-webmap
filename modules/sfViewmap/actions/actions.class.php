<?php

require_once dirname(__FILE__).'/../lib/BasesfViewmapActions.class.php';

/**
 * sfViewmap actions.
 * 
 * @package    sfWebMapPlugin
 * @subpackage sfViewmap
 * @author     Melquisedec Wilchez <CompuDev Web & Hosting>
 * @version    SVN: $Id: actions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
class sfViewmapActions extends BasesfViewmapActions
{
}
