<?php

/**
 * Base actions for the sfWebMapPlugin sfViewmap module.
 * 
 * @package     sfWebMapPlugin
 * @subpackage  sfViewmap
 * @author      Melquisedec Wilchez <CompuDev Web & Hosting>
 * @version     SVN: $Id: BaseActions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
abstract class BasesfViewmapActions extends sfActions
{
}
