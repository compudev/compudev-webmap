<?php

require_once dirname(__FILE__).'/../lib/BasesfAdminmapActions.class.php';

/**
 * sfAdminmap actions.
 * 
 * @package    sfWebMapPlugin
 * @subpackage sfAdminmap
 * @author     Melquisedec Wilchez <CompuDev Web & Hosting>
 * @version    SVN: $Id: actions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
class sfAdminmapActions extends BasesfAdminmapActions
{
}
